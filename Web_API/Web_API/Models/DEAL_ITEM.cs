namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.DEAL_ITEM")]
    public partial class DEAL_ITEM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DEAL_ITEM_ID { get; set; }

        public short? QUANTITY { get; set; }

        [StringLength(50)]
        public string PRODUCT_TYPE { get; set; }

        public int? PRODUCT_SIZE_ID { get; set; }

        public int DEAL_ID { get; set; }

        [JsonIgnore]
        public virtual DEAL DEAL { get; set; }

        [JsonIgnore]
        public virtual PRODUCT_TYPE PRODUCT_TYPE1 { get; set; }

        [JsonIgnore]
        public virtual SIZE_OPTIONS SIZE_OPTIONS { get; set; }
    }
}
