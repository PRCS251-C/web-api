namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.DEAL")]
    public partial class DEAL
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DEAL()
        {
            DEAL_ORDER_ITEM = new HashSet<DEAL_ORDER_ITEM>();
            DEAL_ITEM = new HashSet<DEAL_ITEM>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DEAL_ID { get; set; }

        [StringLength(25)]
        public string DEAL_NAME { get; set; }

        [StringLength(500)]
        public string DEAL_DESCRIPTION { get; set; }

        public decimal? DEAL_PRICE { get; set; }

        public byte? DEAL_PERCENTAGE_DISCOUNT { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DEAL_ORDER_ITEM> DEAL_ORDER_ITEM { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DEAL_ITEM> DEAL_ITEM { get; set; }
    }
}
