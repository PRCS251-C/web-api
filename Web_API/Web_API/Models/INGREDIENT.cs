namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.INGREDIENT")]
    public partial class INGREDIENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INGREDIENT()
        {
            DEFAULT_INGREDIENT_QUANTITY = new HashSet<DEFAULT_INGREDIENT_QUANTITY>();
            ORDER_ITEM_INGREDIENT_QUANTITY = new HashSet<ORDER_ITEM_INGREDIENT_QUANTITY>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int INGREDIENT_ID { get; set; }

        [StringLength(20)]
        public string INGREDIENT_NAME { get; set; }

        [StringLength(500)]
        public string INGREDIENT_DESCRIPTION { get; set; }

        public decimal? INGREDIENT_ADDITIONAL_COST { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DEFAULT_INGREDIENT_QUANTITY> DEFAULT_INGREDIENT_QUANTITY { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_ITEM_INGREDIENT_QUANTITY> ORDER_ITEM_INGREDIENT_QUANTITY { get; set; }
    }
}
