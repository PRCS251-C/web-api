namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.ORDER_ITEM")]
    public partial class ORDER_ITEM
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDER_ITEM()
        {
            DEAL_ORDER_ITEM = new HashSet<DEAL_ORDER_ITEM>();
            ORDER_ITEM_INGREDIENT_QUANTITY = new HashSet<ORDER_ITEM_INGREDIENT_QUANTITY>();
        }

        [Key]
        public decimal ORDER_ITEM_ID { get; set; }

        public short? ORDER_ITEM_QUANTITY { get; set; }

        public int? ORDER_ITEM_SIZE { get; set; }

        public int? ORDER_ID { get; set; }

        public int? PRODUCT_ID { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DEAL_ORDER_ITEM> DEAL_ORDER_ITEM { get; set; }

        [JsonIgnore]
        public virtual ORDERS ORDERS { get; set; }

        [JsonIgnore]
        public virtual PRODUCT PRODUCT { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_ITEM_INGREDIENT_QUANTITY> ORDER_ITEM_INGREDIENT_QUANTITY { get; set; }

        [JsonIgnore]
        public virtual SIZE_OPTIONS SIZE_OPTIONS { get; set; }
    }
}
