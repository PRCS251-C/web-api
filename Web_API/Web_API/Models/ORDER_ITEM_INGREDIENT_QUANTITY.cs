namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.ORDER_ITEM_INGREDIENT_QUANTITY")]
    public partial class ORDER_ITEM_INGREDIENT_QUANTITY
    {
        [Key]
        [Column(Order = 0)]
        public decimal ORDER_ITEM_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int INGREDIENT_ID { get; set; }

        public short? QUANTITY { get; set; }

        [JsonIgnore]
        public virtual INGREDIENT INGREDIENT { get; set; }

        [JsonIgnore]
        public virtual ORDER_ITEM ORDER_ITEM { get; set; }
    }
}
