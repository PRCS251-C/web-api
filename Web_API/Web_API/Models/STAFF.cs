namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.STAFF")]
    public partial class STAFF
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public STAFF()
        {
            ORDERS = new HashSet<ORDERS>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int STAFF_ID { get; set; }

        [StringLength(25)]
        public string STAFF_FIRST_NAME { get; set; }

        [StringLength(25)]
        public string STAFF_LAST_NAME { get; set; }

        [StringLength(100)]
        public string STAFF_EMAIL { get; set; }

        [StringLength(20)]
        public string STAFF_PHONE_NUMBER { get; set; }

        public DateTime? STAFF_DOB { get; set; }

        [StringLength(20)]
        public string STAFF_POSITION { get; set; }

        public int? STAFF_SALARY { get; set; }

        [StringLength(25)]
        public string STAFF_USERNAME { get; set; }

        [StringLength(100)]
        public string STAFF_PASSWORD { get; set; }

        public int? STORE_ID { get; set; }

        [StringLength(100)]
        public string STAFF_SEASONING { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDERS> ORDERS { get; set; }

        [JsonIgnore]
        public virtual STORES STORES { get; set; }
    }
}
