namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.ORDERS")]
    public partial class ORDERS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDERS()
        {
            ORDER_ITEM = new HashSet<ORDER_ITEM>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ORDER_ID { get; set; }

        public decimal ORDER_PRICE { get; set; }

        public DateTime? ORDER_TIME { get; set; }

        [StringLength(50)]
        public string ORDER_STATUS { get; set; }

        public int? CUSTOMER_ID { get; set; }

        public int? PAYMENT_ID { get; set; }

        public int? STAFF_ID { get; set; }

        public string DELIVERY_ADDRESS { get; set; }

        public string DELIVERY_POSTCODE { get; set; }

        [JsonIgnore]
        public virtual CUSTOMER CUSTOMER { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_ITEM> ORDER_ITEM { get; set; }

        [JsonIgnore]
        public virtual PAYMENT PAYMENT { get; set; }

        [JsonIgnore]
        public virtual STAFF STAFF { get; set; }
    }
}
