namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.DEFAULT_INGREDIENT_QUANTITY")]
    public partial class DEFAULT_INGREDIENT_QUANTITY
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PRODUCT_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int INGREDIENT_ID { get; set; }

        public int? QUANTITY { get; set; }

        [JsonIgnore]
        public virtual PRODUCT PRODUCT { get; set; }

        [JsonIgnore]
        public virtual INGREDIENT INGREDIENT { get; set; }
    }
}
