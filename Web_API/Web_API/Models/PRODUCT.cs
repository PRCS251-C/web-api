namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.PRODUCT")]
    public partial class PRODUCT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCT()
        {
            DEFAULT_INGREDIENT_QUANTITY = new HashSet<DEFAULT_INGREDIENT_QUANTITY>();
            ORDER_ITEM = new HashSet<ORDER_ITEM>();
            PRODUCT_SIZES = new HashSet<PRODUCT_SIZES>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PRODUCT_ID { get; set; }

        [StringLength(100)]
        public string PRODUCT_NAME { get; set; }

        [StringLength(500)]
        public string PRODUCT_DESCRIPTION { get; set; }

        public int? PRODUCT_PRICE { get; set; }

        public byte[] PRODUCT_IMAGE { get; set; }

        [StringLength(50)]
        public string PRODUCT_TYPE { get; set; }

        [StringLength(1)]
        public string PRODUCT_IS_CUSTOMISABLE { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DEFAULT_INGREDIENT_QUANTITY> DEFAULT_INGREDIENT_QUANTITY { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_ITEM> ORDER_ITEM { get; set; }

        [JsonIgnore]
        public virtual PRODUCT_TYPE PRODUCT_TYPE1 { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT_SIZES> PRODUCT_SIZES { get; set; }
    }
}
