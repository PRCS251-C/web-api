namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.CUSTOMER")]
    public partial class CUSTOMER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CUSTOMER()
        {
            ORDERS = new HashSet<ORDERS>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CUSTOMER_ID { get; set; }

        [StringLength(25)]
        public string CUSTOMER_FIRST_NAME { get; set; }

        [StringLength(25)]
        public string CUSTOMER_LAST_NAME { get; set; }

        [StringLength(200)]
        public string CUSTOMER_ADDRESS { get; set; }

        [StringLength(10)]
        public string CUSTOMER_POSTCODE { get; set; }

        [StringLength(100)]
        public string CUSTOMER_EMAIL { get; set; }

        public DateTime? CUSTOMER_DOB { get; set; }

        [StringLength(250)]
        public string CUSTOMER_USERNAME { get; set; }

        [StringLength(100)]
        public string CUSTOMER_PASSWORD { get; set; }

        [StringLength(100)]
        public string CUSTOMER_SEASONING { get; set; }

        [StringLength(15)]
        public string CUSTOMER_PHONE_NUMB { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDERS> ORDERS { get; set; }
    }
}
