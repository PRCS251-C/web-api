namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.PRODUCT_SIZES")]
    public partial class PRODUCT_SIZES
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PRODUCT_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SIZE_ID { get; set; }

        public decimal PRICE { get; set; }

        [JsonIgnore]
        public virtual PRODUCT PRODUCT { get; set; }

        [JsonIgnore]
        public virtual SIZE_OPTIONS SIZE_OPTIONS { get; set; }
    }
}
