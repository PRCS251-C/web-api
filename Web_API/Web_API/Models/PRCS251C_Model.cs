namespace Web_API.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PRCS251C_Model : DbContext
    {
        public PRCS251C_Model()
            : base("name=PRCS251C_Model")
        {
        }

        public virtual DbSet<CUSTOMER> CUSTOMER { get; set; }
        public virtual DbSet<DEAL> DEAL { get; set; }
        public virtual DbSet<DEAL_ITEM> DEAL_ITEM { get; set; }
        public virtual DbSet<DEAL_ORDER_ITEM> DEAL_ORDER_ITEM { get; set; }
        public virtual DbSet<DEFAULT_INGREDIENT_QUANTITY> DEFAULT_INGREDIENT_QUANTITY { get; set; }
        public virtual DbSet<INGREDIENT> INGREDIENT { get; set; }
        public virtual DbSet<ORDER_ITEM> ORDER_ITEM { get; set; }
        public virtual DbSet<ORDER_ITEM_INGREDIENT_QUANTITY> ORDER_ITEM_INGREDIENT_QUANTITY { get; set; }
        public virtual DbSet<ORDERS> ORDERS { get; set; }
        public virtual DbSet<PAYMENT> PAYMENT { get; set; }
        public virtual DbSet<PRODUCT> PRODUCT { get; set; }
        public virtual DbSet<PRODUCT_SIZES> PRODUCT_SIZES { get; set; }
        public virtual DbSet<PRODUCT_TYPE> PRODUCT_TYPE { get; set; }
        public virtual DbSet<SIZE_OPTIONS> SIZE_OPTIONS { get; set; }
        public virtual DbSet<STAFF> STAFF { get; set; }
        public virtual DbSet<STORES> STORES { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.CUSTOMER_FIRST_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.CUSTOMER_LAST_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.CUSTOMER_ADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.CUSTOMER_POSTCODE)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.CUSTOMER_EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.CUSTOMER_USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.CUSTOMER_PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.CUSTOMER_SEASONING)
                .IsUnicode(false);

            modelBuilder.Entity<CUSTOMER>()
                .Property(e => e.CUSTOMER_PHONE_NUMB)
                .IsUnicode(false);

            modelBuilder.Entity<DEAL>()
                .Property(e => e.DEAL_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<DEAL>()
                .Property(e => e.DEAL_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<DEAL>()
                .Property(e => e.DEAL_PRICE)
                .HasPrecision(32, 2);

            modelBuilder.Entity<DEAL_ITEM>()
                .Property(e => e.PRODUCT_TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<DEAL_ORDER_ITEM>()
                .Property(e => e.ORDER_ITEM_ID)
                .HasPrecision(20, 0);

            modelBuilder.Entity<INGREDIENT>()
                .Property(e => e.INGREDIENT_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<INGREDIENT>()
                .Property(e => e.INGREDIENT_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<INGREDIENT>()
                .HasMany(e => e.DEFAULT_INGREDIENT_QUANTITY)
                .WithRequired(e => e.INGREDIENT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<INGREDIENT>()
                .HasMany(e => e.ORDER_ITEM_INGREDIENT_QUANTITY)
                .WithRequired(e => e.INGREDIENT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<INGREDIENT>()
                .Property(e => e.INGREDIENT_ADDITIONAL_COST)
                .HasPrecision(32, 2);

            modelBuilder.Entity<ORDER_ITEM>()
                .Property(e => e.ORDER_ITEM_ID)
                .HasPrecision(20, 0);

            modelBuilder.Entity<ORDER_ITEM>()
                .HasMany(e => e.ORDER_ITEM_INGREDIENT_QUANTITY)
                .WithRequired(e => e.ORDER_ITEM)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ORDER_ITEM_INGREDIENT_QUANTITY>()
                .Property(e => e.ORDER_ITEM_ID)
                .HasPrecision(20, 0);

            modelBuilder.Entity<ORDERS>()
                .Property(e => e.ORDER_STATUS)
                .IsUnicode(false);

            modelBuilder.Entity<ORDERS>()
                .Property(e => e.ORDER_PRICE)
                .HasPrecision(32, 2);

            modelBuilder.Entity<ORDERS>()
                .Property(e => e.DELIVERY_ADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<ORDERS>()
                .Property(e => e.DELIVERY_POSTCODE)
                .IsUnicode(false);

            modelBuilder.Entity<PAYMENT>()
                .Property(e => e.PAYMENT_METHOD)
                .IsUnicode(false);

            modelBuilder.Entity<PAYMENT>()
                .Property(e => e.PAYPAL_EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<PAYMENT>()
                .Property(e => e.CARD_TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<PAYMENT>()
                .Property(e => e.CARD_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<PAYMENT>()
                .Property(e => e.CARD_NUMBER)
                .IsUnicode(false);

            modelBuilder.Entity<PAYMENT>()
                .Property(e => e.BILLING_ADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<PAYMENT>()
                .Property(e => e.BILLING_POSTCODE)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCT>()
                .Property(e => e.PRODUCT_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCT>()
                .Property(e => e.PRODUCT_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCT>()
                .Property(e => e.PRODUCT_TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCT>()
                .Property(e => e.PRODUCT_IS_CUSTOMISABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCT>()
                .HasMany(e => e.DEFAULT_INGREDIENT_QUANTITY)
                .WithRequired(e => e.PRODUCT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PRODUCT>()
                .HasMany(e => e.PRODUCT_SIZES)
                .WithRequired(e => e.PRODUCT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PRODUCT_SIZES>()
                .Property(e => e.PRICE)
                .HasPrecision(32, 2);

            modelBuilder.Entity<PRODUCT_TYPE>()
                .Property(e => e.PRODUCT_TYPE_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCT_TYPE>()
                .HasMany(e => e.DEAL_ITEM)
                .WithOptional(e => e.PRODUCT_TYPE1)
                .HasForeignKey(e => e.PRODUCT_TYPE);

            modelBuilder.Entity<PRODUCT_TYPE>()
                .HasMany(e => e.PRODUCT)
                .WithOptional(e => e.PRODUCT_TYPE1)
                .HasForeignKey(e => e.PRODUCT_TYPE);

            modelBuilder.Entity<SIZE_OPTIONS>()
                .Property(e => e.SIZE_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<SIZE_OPTIONS>()
                .HasMany(e => e.DEAL_ITEM)
                .WithOptional(e => e.SIZE_OPTIONS)
                .HasForeignKey(e => e.PRODUCT_SIZE_ID);

            modelBuilder.Entity<SIZE_OPTIONS>()
                .HasMany(e => e.ORDER_ITEM)
                .WithOptional(e => e.SIZE_OPTIONS)
                .HasForeignKey(e => e.ORDER_ITEM_SIZE);

            modelBuilder.Entity<SIZE_OPTIONS>()
                .HasMany(e => e.PRODUCT_SIZES)
                .WithRequired(e => e.SIZE_OPTIONS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<STAFF>()
                .Property(e => e.STAFF_FIRST_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<STAFF>()
                .Property(e => e.STAFF_LAST_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<STAFF>()
                .Property(e => e.STAFF_EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<STAFF>()
                .Property(e => e.STAFF_PHONE_NUMBER)
                .IsUnicode(false);

            modelBuilder.Entity<STAFF>()
                .Property(e => e.STAFF_POSITION)
                .IsUnicode(false);

            modelBuilder.Entity<STAFF>()
                .Property(e => e.STAFF_USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<STAFF>()
                .Property(e => e.STAFF_PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<STAFF>()
                .Property(e => e.STAFF_SEASONING)
                .IsUnicode(false);

            modelBuilder.Entity<STORES>()
                .Property(e => e.STORE_ADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<STORES>()
                .Property(e => e.STORE_POSTCODE)
                .IsUnicode(false);

            modelBuilder.Entity<STORES>()
                .Property(e => e.STORE_PHONE_NUMBER)
                .IsUnicode(false);
        }
    }
}
