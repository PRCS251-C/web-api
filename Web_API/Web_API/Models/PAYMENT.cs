namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.PAYMENT")]
    public partial class PAYMENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PAYMENT()
        {
            ORDERS = new HashSet<ORDERS>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PAYMENT_ID { get; set; }

        [StringLength(20)]
        public string PAYMENT_METHOD { get; set; }

        [StringLength(100)]
        public string PAYPAL_EMAIL { get; set; }

        [StringLength(20)]
        public string CARD_TYPE { get; set; }

        public string CARD_NUMBER { get; set; }

        [StringLength(50)]
        public string CARD_NAME { get; set; }

        public DateTime? CARD_ISSUE_DATE { get; set; }

        public DateTime? CARD_EXPIRY_DATE { get; set; }

        public byte? CARD_CVC { get; set; }

        public byte? CARD_ISSUE_NUMBER { get; set; }

        [StringLength(200)]
        public string BILLING_ADDRESS { get; set; }

        [StringLength(10)]
        public string BILLING_POSTCODE { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDERS> ORDERS { get; set; }
    }
}
