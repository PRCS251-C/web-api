namespace Web_API.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRCS251C.DEAL_ORDER_ITEM")]
    public partial class DEAL_ORDER_ITEM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DEAL_ORDER_ITEM_ID { get; set; }

        public int? DEAL_ID { get; set; }

        public decimal? ORDER_ITEM_ID { get; set; }

        [JsonIgnore]
        public virtual DEAL DEAL { get; set; }

        [JsonIgnore]
        public virtual ORDER_ITEM ORDER_ITEM { get; set; }
    }
}
