﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace Web_API
{
    public class EncryptionHandler
    {
        private static EncryptionHandler instance;

        public RSAParameters publicKey { get; private set; }
        private RSAParameters privateKey { get; set; }
        private byte[] AESKey { set; get; }
        private byte[] AESIV { set; get; }
        private string AESPadding { set; get; }

        private Dictionary<string, string> allowedApps;

        protected EncryptionHandler(RSAParameters publicKey, RSAParameters privateKey, RijndaelManaged AESCrypto)
        {
            this.publicKey = publicKey;
            this.privateKey = privateKey;
            this.AESKey = AESCrypto.Key;
            this.AESIV = AESCrypto.IV;
            this.AESPadding = AESCrypto.Padding.ToString();
        }

        public static EncryptionHandler getInstance()
        {
            if (instance == null)
            {
                var crypto = new RSACryptoServiceProvider(4096);
                var privKey = crypto.ExportParameters(true);
                var pubKey = crypto.ExportParameters(false);

                RijndaelManaged AESCrypto = new RijndaelManaged();
                AESCrypto.KeySize = 256;

                instance = new EncryptionHandler(pubKey, privKey, AESCrypto);
            }

            return instance;
        }

        public byte[] EncryptWithAES(string data)
        {
            RijndaelManaged crypto = null;
            MemoryStream memStream = null;
            ICryptoTransform encrypter = null;
            CryptoStream cryptoStream = null;

            UTF8Encoding encoding = new UTF8Encoding();
            var dataBytes = encoding.GetBytes(data);

            try
            {
                crypto = new RijndaelManaged();
                crypto.Key = AESKey;
                crypto.IV = AESIV;

                memStream = new MemoryStream();
                encrypter = crypto.CreateEncryptor(AESKey, AESIV);
                cryptoStream = new CryptoStream(memStream, encrypter, CryptoStreamMode.Write);

                cryptoStream.Write(dataBytes, 0, dataBytes.Length);
                cryptoStream.FlushFinalBlock();
            }
            finally
            {
                if (crypto != null)
                {
                    crypto.Clear();
                }
            }
            return memStream.ToArray();
        }

        public string DecryptWithAES(byte[] data)
        {
            RijndaelManaged crypto = null;
            MemoryStream memStream = null;
            ICryptoTransform decrypter = null;
            CryptoStream cryptoStream = null;
            string decryptedString;

            try
            {
                crypto = new RijndaelManaged();
                crypto.Key = AESKey;
                crypto.IV = AESIV;

                memStream = new MemoryStream(data);
                decrypter = crypto.CreateDecryptor(AESKey, AESIV);
                cryptoStream = new CryptoStream(memStream, decrypter, CryptoStreamMode.Read);

                StreamReader streamReader = new StreamReader(cryptoStream);
                decryptedString = streamReader.ReadToEnd();
            }
            finally
            {
                if (crypto != null)
                {
                    crypto.Clear();
                    memStream.Close();
                    memStream.Flush();
                }
            }
            return decryptedString;
        }

        public string EncryptWithRSA(string data, string recipientPubKeyString)
        {
            var recipientPubKey = stringToKey(recipientPubKeyString);

            var crypto = new RSACryptoServiceProvider();
            crypto.ImportParameters(recipientPubKey);
            var dataBytes = Encoding.Unicode.GetBytes(data);
            var encryptedBytes = crypto.Encrypt(dataBytes, false);

            return Convert.ToBase64String(encryptedBytes);
        }

        public string DecryptWithRSA(string data)
        {
            var dataBytes = Convert.FromBase64String(data);
            var crypto = new RSACryptoServiceProvider();
            crypto.ImportParameters(this.privateKey);
            var decryptedBytes = crypto.Decrypt(dataBytes, false);

            return Encoding.Unicode.GetString(decryptedBytes);
        }

        public string keyToString(RSAParameters key)
        {
            var sw = new StringWriter();
            var xs = new XmlSerializer(typeof(RSAParameters));
            xs.Serialize(sw, key);
            return sw.ToString();
        }

        public RSAParameters stringToKey(string key)
        {
            var sr = new StringReader(key);
            var xs = new XmlSerializer(typeof(RSAParameters));
            return (RSAParameters)xs.Deserialize(sr);
        }

        public IEnumerable<string> getAESKey()
        {
            return new string[] { Convert.ToBase64String(this.AESKey), Convert.ToBase64String(AESIV), AESPadding };
        }
    }
}