﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class SizeOptionsController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/SizeOptions
        public IQueryable<SIZE_OPTIONS> GetSIZE_OPTIONS()
        {
            return db.SIZE_OPTIONS;
        }

        // GET: api/SizeOptions/5
        [ResponseType(typeof(SIZE_OPTIONS))]
        public IHttpActionResult GetSIZE_OPTIONS(int id)
        {
            SIZE_OPTIONS sIZE_OPTIONS = db.SIZE_OPTIONS.Find(id);
            if (sIZE_OPTIONS == null)
            {
                return NotFound();
            }

            return Ok(sIZE_OPTIONS);
        }

        // PUT: api/SizeOptions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSIZE_OPTIONS(int id, SIZE_OPTIONS sIZE_OPTIONS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sIZE_OPTIONS.SIZE_ID)
            {
                return BadRequest();
            }

            db.Entry(sIZE_OPTIONS).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SIZE_OPTIONSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SizeOptions
        [ResponseType(typeof(SIZE_OPTIONS))]
        public IHttpActionResult PostSIZE_OPTIONS(SIZE_OPTIONS sIZE_OPTIONS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SIZE_OPTIONS.Add(sIZE_OPTIONS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SIZE_OPTIONSExists(sIZE_OPTIONS.SIZE_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = sIZE_OPTIONS.SIZE_ID }, sIZE_OPTIONS);
        }

        // DELETE: api/SizeOptions/5
        [ResponseType(typeof(SIZE_OPTIONS))]
        public IHttpActionResult DeleteSIZE_OPTIONS(int id)
        {
            SIZE_OPTIONS sIZE_OPTIONS = db.SIZE_OPTIONS.Find(id);
            if (sIZE_OPTIONS == null)
            {
                return NotFound();
            }

            db.SIZE_OPTIONS.Remove(sIZE_OPTIONS);
            db.SaveChanges();

            return Ok(sIZE_OPTIONS);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SIZE_OPTIONSExists(int id)
        {
            return db.SIZE_OPTIONS.Count(e => e.SIZE_ID == id) > 0;
        }
    }
}