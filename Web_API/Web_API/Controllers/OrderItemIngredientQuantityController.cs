﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class OrderItemIngredientQuantityController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/OrderItemIngredientQuantity
        public IQueryable<ORDER_ITEM_INGREDIENT_QUANTITY> GetORDER_ITEM_INGREDIENT_QUANTITY()
        {
            return db.ORDER_ITEM_INGREDIENT_QUANTITY;
        }

        // GET: api/OrderItemIngredientQuantity/5
        [ResponseType(typeof(ORDER_ITEM_INGREDIENT_QUANTITY))]
        public IHttpActionResult GetORDER_ITEM_INGREDIENT_QUANTITY(decimal id)
        {
            ORDER_ITEM_INGREDIENT_QUANTITY oRDER_ITEM_INGREDIENT_QUANTITY = db.ORDER_ITEM_INGREDIENT_QUANTITY.Find(id);
            if (oRDER_ITEM_INGREDIENT_QUANTITY == null)
            {
                return NotFound();
            }

            return Ok(oRDER_ITEM_INGREDIENT_QUANTITY);
        }

        // WARNING: UNTESTED
        // GET: api/OrderItem/{id}/Ingredients
        [Route("api/OrderItem/{id}/Ingredients/Custom")]
        [ResponseType(typeof(ORDER_ITEM_INGREDIENT_QUANTITY))]
        public IQueryable<ORDER_ITEM_INGREDIENT_QUANTITY> GetOrderItemIngredients(int id)
        {
            return db.ORDER_ITEM_INGREDIENT_QUANTITY.Where(b => b.ORDER_ITEM_ID.Equals(id));
        }

        // PUT: api/OrderItemIngredientQuantity/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutORDER_ITEM_INGREDIENT_QUANTITY(decimal id, ORDER_ITEM_INGREDIENT_QUANTITY oRDER_ITEM_INGREDIENT_QUANTITY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != oRDER_ITEM_INGREDIENT_QUANTITY.ORDER_ITEM_ID)
            {
                return BadRequest();
            }

            db.Entry(oRDER_ITEM_INGREDIENT_QUANTITY).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ORDER_ITEM_INGREDIENT_QUANTITYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/OrderItemIngredientQuantity
        [ResponseType(typeof(ORDER_ITEM_INGREDIENT_QUANTITY))]
        public IHttpActionResult PostORDER_ITEM_INGREDIENT_QUANTITY(ORDER_ITEM_INGREDIENT_QUANTITY oRDER_ITEM_INGREDIENT_QUANTITY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ORDER_ITEM_INGREDIENT_QUANTITY.Add(oRDER_ITEM_INGREDIENT_QUANTITY);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ORDER_ITEM_INGREDIENT_QUANTITYExists(oRDER_ITEM_INGREDIENT_QUANTITY.ORDER_ITEM_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = oRDER_ITEM_INGREDIENT_QUANTITY.ORDER_ITEM_ID }, oRDER_ITEM_INGREDIENT_QUANTITY);
        }

        // DELETE: api/OrderItemIngredientQuantity/5
        [ResponseType(typeof(ORDER_ITEM_INGREDIENT_QUANTITY))]
        public IHttpActionResult DeleteORDER_ITEM_INGREDIENT_QUANTITY(decimal id)
        {
            ORDER_ITEM_INGREDIENT_QUANTITY oRDER_ITEM_INGREDIENT_QUANTITY = db.ORDER_ITEM_INGREDIENT_QUANTITY.Find(id);
            if (oRDER_ITEM_INGREDIENT_QUANTITY == null)
            {
                return NotFound();
            }

            db.ORDER_ITEM_INGREDIENT_QUANTITY.Remove(oRDER_ITEM_INGREDIENT_QUANTITY);
            db.SaveChanges();

            return Ok(oRDER_ITEM_INGREDIENT_QUANTITY);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ORDER_ITEM_INGREDIENT_QUANTITYExists(decimal id)
        {
            return db.ORDER_ITEM_INGREDIENT_QUANTITY.Count(e => e.ORDER_ITEM_ID == id) > 0;
        }
    }
}