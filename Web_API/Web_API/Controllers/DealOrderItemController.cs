﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class DealOrderItemController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/DealOrderItem
        public IQueryable<DEAL_ORDER_ITEM> GetDEAL_ORDER_ITEM()
        {
            return db.DEAL_ORDER_ITEM;
        }

        // GET: api/DealOrderItem/5
        [ResponseType(typeof(DEAL_ORDER_ITEM))]
        public IHttpActionResult GetDEAL_ORDER_ITEM(int id)
        {
            DEAL_ORDER_ITEM dEAL_ORDER_ITEM = db.DEAL_ORDER_ITEM.Find(id);
            if (dEAL_ORDER_ITEM == null)
            {
                return NotFound();
            }

            return Ok(dEAL_ORDER_ITEM);
        }

        // PUT: api/DealOrderItem/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDEAL_ORDER_ITEM(int id, DEAL_ORDER_ITEM dEAL_ORDER_ITEM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dEAL_ORDER_ITEM.DEAL_ORDER_ITEM_ID)
            {
                return BadRequest();
            }

            db.Entry(dEAL_ORDER_ITEM).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DEAL_ORDER_ITEMExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DealOrderItem
        [ResponseType(typeof(DEAL_ORDER_ITEM))]
        public IHttpActionResult PostDEAL_ORDER_ITEM(DEAL_ORDER_ITEM dEAL_ORDER_ITEM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DEAL_ORDER_ITEM.Add(dEAL_ORDER_ITEM);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DEAL_ORDER_ITEMExists(dEAL_ORDER_ITEM.DEAL_ORDER_ITEM_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = dEAL_ORDER_ITEM.DEAL_ORDER_ITEM_ID }, dEAL_ORDER_ITEM);
        }

        // DELETE: api/DealOrderItem/5
        [ResponseType(typeof(DEAL_ORDER_ITEM))]
        public IHttpActionResult DeleteDEAL_ORDER_ITEM(int id)
        {
            DEAL_ORDER_ITEM dEAL_ORDER_ITEM = db.DEAL_ORDER_ITEM.Find(id);
            if (dEAL_ORDER_ITEM == null)
            {
                return NotFound();
            }

            db.DEAL_ORDER_ITEM.Remove(dEAL_ORDER_ITEM);
            db.SaveChanges();

            return Ok(dEAL_ORDER_ITEM);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DEAL_ORDER_ITEMExists(int id)
        {
            return db.DEAL_ORDER_ITEM.Count(e => e.DEAL_ORDER_ITEM_ID == id) > 0;
        }
    }
}