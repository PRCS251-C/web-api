﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class StaffController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/Staff
        public IQueryable<STAFF> GetSTAFF()
        {
            return db.STAFF;
        }

        // GET: api/Staff/5
        [ResponseType(typeof(STAFF))]
        public IHttpActionResult GetSTAFF(int id)
        {
            STAFF sTAFF = db.STAFF.Find(id);
            if (sTAFF == null)
            {
                return NotFound();
            }

            return Ok(sTAFF);
        }

        // GET: api/Staff/{username}
        [Route("api/Staff/Username/{username}")]
        [ResponseType(typeof(STAFF))]
        public IQueryable<STAFF> getStaffByUsername(string username)
        {
            return db.STAFF.Where(b => b.STAFF_USERNAME.Equals(username));
        }

        [Route("api/Staff/{username}/Position")]
        public IHttpActionResult getStaffPosition(string username)
        {
            return Ok(db.STAFF.Where(b => b.STAFF_USERNAME.Equals(username)).Select(c => c.STAFF_POSITION));
        }

        // GET: api/Staff/{username}/Salt
        [Route("api/Staff/{username}/Salt")]
        public IHttpActionResult getStaffSalt(string username)
        {
            return Ok(db.STAFF.Where(b => b.STAFF_USERNAME.Equals(username)).Select(c => c.STAFF_SEASONING));
        }

        [Route("api/Staff/Login")]
        public IHttpActionResult login(dynamic json)
        {
            JObject root = json;

            string username = (string)root["username"];
            string password = (string)root["password"];

            string storedPassword = db.STAFF.Where(b => b.STAFF_USERNAME.Equals(username)).Select(c => c.STAFF_PASSWORD).First();

            if (password.Equals(storedPassword))
            {
                return Ok();
            } else
            {
                return Unauthorized();
            }
        }

        // PUT: api/Staff/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSTAFF(int id, STAFF sTAFF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sTAFF.STAFF_ID)
            {
                return BadRequest();
            }

            db.Entry(sTAFF).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!STAFFExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Staff
        [ResponseType(typeof(STAFF))]
        public IHttpActionResult PostSTAFF(STAFF sTAFF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.STAFF.Add(sTAFF);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (STAFFExists(sTAFF.STAFF_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(getLastId());
        }

        // DELETE: api/Staff/5
        [ResponseType(typeof(STAFF))]
        public IHttpActionResult DeleteSTAFF(int id)
        {
            STAFF sTAFF = db.STAFF.Find(id);
            if (sTAFF == null)
            {
                return NotFound();
            }

            db.STAFF.Remove(sTAFF);
            db.SaveChanges();

            return Ok(sTAFF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Route("api/Staff/LastId")]
        public int getLastId()
        {
            return db.STAFF.Select(b => b.STAFF_ID).Max();
        }

        private bool STAFFExists(int id)
        {
            return db.STAFF.Count(e => e.STAFF_ID == id) > 0;
        }
    }
}