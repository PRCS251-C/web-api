﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class DealController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/Deal
        public IQueryable<DEAL> GetDEAL()
        {
            return db.DEAL;
        }

        // GET: api/Deal/5
        [ResponseType(typeof(DEAL))]
        public IHttpActionResult GetDEAL(int id)
        {
            DEAL dEAL = db.DEAL.Find(id);
            if (dEAL == null)
            {
                return NotFound();
            }

            return Ok(dEAL);
        }

        [Route("api/Deal/{id}/Items")]
        [ResponseType(typeof(DEAL_ITEM))]
        public IEnumerable<DEAL_ITEM> getDealItems(int id)
        {
            return db.DEAL_ITEM.Where(b => b.DEAL_ID.Equals(id));
        }

        [ResponseType(typeof(int))]
        [Route("api/Deal/LastId")]
        public int getLastId()
        {
            return db.DEAL.Select(b => b.DEAL_ID).Max();
        }

        // PUT: api/Deal/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDEAL(int id, DEAL dEAL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dEAL.DEAL_ID)
            {
                return BadRequest();
            }

            db.Entry(dEAL).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DEALExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("api/Deal/PutAsWhole")]
        [HttpPut]
        public IHttpActionResult putDeal(dynamic json)
        {
            List<DEAL_ITEM> dealItemList = new List<DEAL_ITEM>();

            JObject root = json;

            JArray dealItems = (JArray)root["DEAL_ITEMS"];

            for (int i = 0; i < dealItems.Count; i++)
            {
                DEAL_ITEM item = dealItems[i].ToObject<DEAL_ITEM>();
                dealItemList.Add(item);
            }

            DEAL deal = root.ToObject<DEAL>();

            IEnumerable<DEAL_ITEM> currentDealItems = getDealItems(deal.DEAL_ID);

            //if (currentDealItems.Any())
            //{
            //    return Ok();
            //}

            ////return Ok(currentDealItems.Count()); <-- Throws error, no idea why

            //for (int i = 0; i < currentDealItems.Count(); i++)
            //{
            //    //currentDealItemsList.Add(currentDealItems.ElementAt(i));
            //}

            IHttpActionResult result = Ok();

            result = PutDEAL(deal.DEAL_ID, deal);

            if (result.Equals(BadRequest()) || result.Equals(Conflict()))
            {
                return result;
            }


            foreach (DEAL_ITEM oldItem in currentDealItems)
            {
                bool isContained = false;

                foreach (DEAL_ITEM item in dealItemList)
                {
                    if (oldItem.DEAL_ITEM_ID.Equals(item.DEAL_ITEM_ID))
                    {
                        isContained = true;
                    }
                }

                if (!isContained)
                {
                    result = new DealItemController().DeleteDEAL_ITEM(oldItem.DEAL_ITEM_ID);
                }

                if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                {
                    return result;
                }
            }

            foreach (DEAL_ITEM item in dealItemList)
            {
                bool isContained = false;

                foreach (DEAL_ITEM oldItem in currentDealItems)
                {
                    if (item.DEAL_ITEM_ID.Equals(oldItem.DEAL_ITEM_ID))
                    {
                        isContained = true;
                    }
                }

                if (isContained)
                {
                    result = new DealItemController().PutDEAL_ITEM(item.DEAL_ITEM_ID, item);
                }
                else
                {
                    result = new DealItemController().PostDEAL_ITEM(item);
                }
         
                if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                {
                    return result;
                }
            }

            return Ok();
        }

        // POST: api/Deal
        [ResponseType(typeof(DEAL))]
        public IHttpActionResult PostDEAL(DEAL dEAL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DEAL.Add(dEAL);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DEALExists(dEAL.DEAL_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = dEAL.DEAL_ID }, dEAL);
        }

        [Route("api/Deal/PostAsWhole")]
        [HttpPost]
        public IHttpActionResult postOrder(dynamic json)
        {
            List<DEAL_ITEM> dealItemList = new List<DEAL_ITEM>();

            JObject root = json;

            JArray dealItems = (JArray)root["DEAL_ITEMS"];

            for (int i = 0; i < dealItems.Count; i++)
            {
                DEAL_ITEM item = dealItems[i].ToObject<DEAL_ITEM>();
                dealItemList.Add(item);
            }

            DEAL deal = root.ToObject<DEAL>();

            IHttpActionResult dealResult = PostDEAL(deal);

            int dealId = getLastId();

            if (dealResult.Equals(BadRequest()) || dealResult.Equals(Conflict()))
            {
                return BadRequest();
            }

            foreach (DEAL_ITEM item in dealItemList)
            {
                item.DEAL_ID = dealId;

                IHttpActionResult result = new DealItemController().PostDEAL_ITEM(item);

                if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                {
                    return BadRequest();
                }
            }

            return Ok(dealId);
        }

        // DELETE: api/Deal/5
        [ResponseType(typeof(DEAL))]
        public IHttpActionResult DeleteDEAL(int id)
        {
            DEAL dEAL = db.DEAL.Find(id);
            if (dEAL == null)
            {
                return NotFound();
            }

            db.DEAL.Remove(dEAL);
            db.SaveChanges();

            return Ok(dEAL);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DEALExists(int id)
        {
            return db.DEAL.Count(e => e.DEAL_ID == id) > 0;
        }
    }
}