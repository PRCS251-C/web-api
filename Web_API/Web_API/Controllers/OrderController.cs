﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Http.Results;

namespace Web_API.Controllers
{
    public class OrderController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/Order
        public IQueryable<ORDERS> GetORDERS()
        {
            return db.ORDERS.Where(b => b.ORDER_STATUS != "Fulfilled" && b.ORDER_STATUS != "Issue Delivering" && b.ORDER_STATUS != "Cancelled");
        }

        // GET: api/Order/5
        [ResponseType(typeof(ORDERS))]
        public IHttpActionResult GetORDERS(int id)
        {
            ORDERS oRDERS = db.ORDERS.Find(id);
            if (oRDERS == null)
            {
                return NotFound();
            }

            return Ok(oRDERS);
        }
        
        [ResponseType(typeof(ORDERS))]
        [Route("api/Order/AwaitingDelivery")]
        public IQueryable<ORDERS> getOrdersAwaitingDelivery()
        {
            return db.ORDERS.Where(b => b.ORDER_STATUS.Equals("Awaiting Delivery"));
        }

        [ResponseType(typeof(int))]
        [Route("api/Order/Last")]
        public int getLastId()
        {
            return db.ORDERS.Select(b => b.ORDER_ID).Max();
        }

        // PUT: api/Order/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutORDERS(int id, ORDERS oRDERS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != oRDERS.ORDER_ID)
            {
                return BadRequest();
            }

            db.Entry(oRDERS).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ORDERSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("api/Order/SetCourier")]
        [HttpPut]
        public IHttpActionResult putOrderCourier(dynamic json)
        {
            JObject root = json;

            int orderId = (int)root["orderId"];
            int staffId = new StaffController().getStaffByUsername((string)root["staffUsername"]).First().STAFF_ID;

            IHttpActionResult orderResult = GetORDERS(orderId);

            var orderContent = orderResult as OkNegotiatedContentResult<ORDERS>;

            ORDERS order = orderContent.Content;

            if (order.STAFF_ID != null)
            {
                return BadRequest();
            }

            order.STAFF_ID = staffId;
            order.ORDER_STATUS = "Out For Delivery";

            IHttpActionResult result = PutORDERS(orderId, order);

            return result;
        }

        // Allows updating an entire order at once, including payment details, order item details and any order item customisations
        // Eliminates risk of network issue on the client side resulting in an incomplete update
        // PUT: api/Order/PutAsWhole
        [Route("api/Order/PutAsWhole")]
        [HttpPut]
        public IHttpActionResult putOrder(dynamic json)
        {
            DateTime startTime = DateTime.Now;

            while (BlockingQueue.getInstance().isOrderPutBlocked)
            {
                //wait for previous order to update fully
                DateTime currentTime = DateTime.Now;

                if ((currentTime - startTime).TotalSeconds >= 60)
                {
                    BlockingQueue.getInstance().isOrderPutBlocked = false;
                    return BadRequest();
                }
            }

            BlockingQueue.getInstance().isOrderPutBlocked = true;

            List<ORDER_ITEM> orderItemList = new List<ORDER_ITEM>();

            JObject root = json;

            JArray orderItems = (JArray)root["ORDER_ITEMS"];

            for (int i = 0; i < orderItems.Count; i++)
            {
                ORDER_ITEM item = orderItems[i].ToObject<ORDER_ITEM>();
                orderItemList.Add(item);
            }

            ORDERS order = root.ToObject<ORDERS>();

            try
            {
                PAYMENT payment = root["payment"].ToObject<PAYMENT>();

                if (payment != null)
                {
                    IHttpActionResult result = new PaymentController().PutPAYMENT(payment.PAYMENT_ID, payment);

                    if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                    {
                        BlockingQueue.getInstance().isOrderPutBlocked = false;
                        return BadRequest();
                    }
                    order.PAYMENT_ID = new PaymentController().getLastId();
                }
            }
            catch (JsonSerializationException)
            {
                //payment info not present
            }

            IHttpActionResult orderResult = PutORDERS(order.ORDER_ID, order);

            if (orderResult.Equals(BadRequest()) || orderResult.Equals(Conflict()))
            {
                BlockingQueue.getInstance().isOrderPutBlocked = false;
                return BadRequest();
            }

            foreach (ORDER_ITEM item in orderItemList)
            {
                IHttpActionResult result = new OrderItemController().PutORDER_ITEM(item.ORDER_ITEM_ID, item);

                if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                {
                    BlockingQueue.getInstance().isOrderPutBlocked = false;
                    return BadRequest();
                }
            }

            try
            {
                for (int i = 0; i < orderItems.Count; i++)
                {
                    JArray customIngredients = (JArray)orderItems[i]["CUSTOM_INGREDIENTS"];

                    for (int j = 0; j < customIngredients.Count; j++)
                    {
                        ORDER_ITEM_INGREDIENT_QUANTITY customIngredient = new ORDER_ITEM_INGREDIENT_QUANTITY();

                        customIngredient.INGREDIENT_ID = (int)customIngredients[j]["INGREDIENT_ID"];
                        customIngredient.ORDER_ITEM_ID = (int)orderItems[i]["ORDER_ID"];
                        customIngredient.QUANTITY = (short)customIngredients[j]["quantity"];

                        IHttpActionResult result = new OrderItemIngredientQuantityController().PutORDER_ITEM_INGREDIENT_QUANTITY(customIngredient.INGREDIENT_ID, customIngredient);

                        if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                        {
                            BlockingQueue.getInstance().isOrderPutBlocked = false;
                            return result;
                        }
                    }
                }
            }
            catch (JsonSerializationException)
            {
                //No custom ingredients
            }

            BlockingQueue.getInstance().isOrderPutBlocked = false;

            return Ok();
        }

        // POST: api/Order
        [ResponseType(typeof(ORDERS))]
        public IHttpActionResult PostORDERS(ORDERS oRDERS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ORDERS.Add(oRDERS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ORDERSExists(oRDERS.ORDER_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = oRDERS.ORDER_ID }, oRDERS);
        }

        // Allows posting an entire order at once, including payment details, order item details and any order item customisations
        // Reduces risk of conflict when multiple users post an order at the same time, and eliminates the risk of a network issue
        // on the clients end resulting in incomplete order data being committed to the DB (would lead to confusion in the kitchen)
        // POST: api/Order/PostAsWhole
        [Route("api/Order/PostAsWhole")]
        [HttpPost]
        public IHttpActionResult postOrder(dynamic json)
        {
            DateTime startTime = DateTime.Now;

            while(BlockingQueue.getInstance().isOrderPostBlocked)
            {
                //wait for previous order to post fully
                DateTime currentTime = DateTime.Now;

                if ((currentTime - startTime).TotalSeconds >= 60)
                {
                    BlockingQueue.getInstance().isOrderPostBlocked = false;
                    return BadRequest();
                }
            }

            BlockingQueue.getInstance().isOrderPostBlocked = true;

            List<ORDER_ITEM> orderItemList = new List<ORDER_ITEM>();

            JObject root = json;

            JArray orderItems = (JArray)root["ORDER_ITEMS"];

            for (int i = 0; i < orderItems.Count; i++)
            {
                ORDER_ITEM item = orderItems[i].ToObject<ORDER_ITEM>();
                orderItemList.Add(item);
            }

            ORDERS order = root.ToObject<ORDERS>();

            try
            {
                PAYMENT payment = root["payment"].ToObject<PAYMENT>();

                if (payment != null)
                {
                    IHttpActionResult result = new PaymentController().PutPAYMENT(payment.PAYMENT_ID, payment);

                    if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                    {
                        BlockingQueue.getInstance().isOrderPostBlocked = false;
                        return BadRequest();
                    }
                    order.PAYMENT_ID = new PaymentController().getLastId();
                }
            }
            catch(JsonSerializationException)
            {
                //payment info not present
            }

            IHttpActionResult orderResult = PostORDERS(order);

            if (orderResult.Equals(BadRequest()) || orderResult.Equals(Conflict()))
            {
                BlockingQueue.getInstance().isOrderPostBlocked = false;
                return BadRequest();
            }

            int orderId = getLastId();

            foreach (ORDER_ITEM item in orderItemList)
            {
                item.ORDER_ID = orderId;

                IHttpActionResult result = new OrderItemController().PostORDER_ITEM(item);

                if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                {
                    BlockingQueue.getInstance().isOrderPostBlocked = false;
                    return result;
                }
            }

            try
            {
                for (int i = 0; i < orderItems.Count; i++)
                {
                    JArray customIngredients = (JArray)orderItems[i]["CUSTOM_INGREDIENTS"];

                    for (int j = 0; j < customIngredients.Count; j++)
                    {
                        ORDER_ITEM_INGREDIENT_QUANTITY customIngredient = new ORDER_ITEM_INGREDIENT_QUANTITY();

                        customIngredient.INGREDIENT_ID = (int)customIngredients[j]["INGREDIENT_ID"];
                        customIngredient.ORDER_ITEM_ID = (int)orderItems[i]["ORDER_ID"];
                        customIngredient.QUANTITY = (short)customIngredients[j]["quantity"];

                        IHttpActionResult result = new OrderItemIngredientQuantityController().PutORDER_ITEM_INGREDIENT_QUANTITY(customIngredient.INGREDIENT_ID, customIngredient);

                        if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                        {
                            BlockingQueue.getInstance().isOrderPostBlocked = false;
                            return result;
                        }
                    }
                }
            } catch (JsonSerializationException)
            {
                //No custom ingredients
            }

            BlockingQueue.getInstance().isOrderPostBlocked = false;

            return Ok(orderId);
        }

        // DELETE: api/Order/5
        [ResponseType(typeof(ORDERS))]
        public IHttpActionResult DeleteORDERS(int id)
        {
            ORDERS oRDERS = db.ORDERS.Find(id);
            if (oRDERS == null)
            {
                return NotFound();
            }

            db.ORDERS.Remove(oRDERS);
            db.SaveChanges();

            return Ok(oRDERS);
        }

        [HttpGet]
        [Route("api/Order/RemoveBlocks")]
        public IHttpActionResult removeBlocks()
        {
            BlockingQueue.getInstance().isOrderPostBlocked = false;
            BlockingQueue.getInstance().isOrderPutBlocked = false;

            if (BlockingQueue.getInstance().isOrderPostBlocked || BlockingQueue.getInstance().isOrderPutBlocked)
            {
                return BadRequest();
            }
            else
            {
                return Ok("Complete");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ORDERSExists(int id)
        {
            return db.ORDERS.Count(e => e.ORDER_ID == id) > 0;
        }
    }
}