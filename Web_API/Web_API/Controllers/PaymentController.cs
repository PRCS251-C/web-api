﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class PaymentController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/Payment
        public IQueryable<PAYMENT> GetPAYMENT()
        {
            return db.PAYMENT;
        }

        // GET: api/Payment/5
        [ResponseType(typeof(PAYMENT))]
        public IHttpActionResult GetPAYMENT(int id)
        {
            PAYMENT pAYMENT = db.PAYMENT.Find(id);
            if (pAYMENT == null)
            {
                return NotFound();
            }

            return Ok(pAYMENT);
        }

        [ResponseType(typeof(int))]
        [Route("api/Payment/Last")]
        public int getLastId()
        {
            return db.PAYMENT.Select(b => b.PAYMENT_ID).Max();
        }

        // PUT: api/Payment/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPAYMENT(int id, PAYMENT pAYMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pAYMENT.PAYMENT_ID)
            {
                return BadRequest();
            }

            db.Entry(pAYMENT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PAYMENTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Payment
        [ResponseType(typeof(PAYMENT))]
        public IHttpActionResult PostPAYMENT(PAYMENT pAYMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PAYMENT.Add(pAYMENT);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PAYMENTExists(pAYMENT.PAYMENT_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(getLastId());
        }

        // DELETE: api/Payment/5
        [ResponseType(typeof(PAYMENT))]
        public IHttpActionResult DeletePAYMENT(int id)
        {
            PAYMENT pAYMENT = db.PAYMENT.Find(id);
            if (pAYMENT == null)
            {
                return NotFound();
            }

            db.PAYMENT.Remove(pAYMENT);
            db.SaveChanges();

            return Ok(pAYMENT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PAYMENTExists(int id)
        {
            return db.PAYMENT.Count(e => e.PAYMENT_ID == id) > 0;
        }
    }
}