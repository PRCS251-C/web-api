﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class DefaultIngredientQuantityController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/DefaultIngredientQuantity
        public IQueryable<DEFAULT_INGREDIENT_QUANTITY> GetDEFAULT_INGREDIENT_QUANTITY()
        {
            return db.DEFAULT_INGREDIENT_QUANTITY;
        }

        // GET: api/DefaultIngredientQuantity/{productId}/{ingredientId}
        [Route("api/DefaultIngredientQuantity/{productId}/{ingredientId}")]
        public IHttpActionResult GetDEFAULT_INGREDIENT_QUANTITY(int productId, int ingredientId)
        {
            DEFAULT_INGREDIENT_QUANTITY dEFAULT_INGREDIENT_QUANTITY = db.DEFAULT_INGREDIENT_QUANTITY.Find(productId, ingredientId);
            if (dEFAULT_INGREDIENT_QUANTITY == null)
            {
                return NotFound();
            }

            return Ok(dEFAULT_INGREDIENT_QUANTITY);
        }

        // GET: api/OrderItem/{id}/Ingredients
        [Route("api/Product/{id}/Ingredients/Default")]
        [ResponseType(typeof(DEFAULT_INGREDIENT_QUANTITY))]
        public IQueryable<DEFAULT_INGREDIENT_QUANTITY> GetOrderItemIngredients(int id)
        {
            return db.DEFAULT_INGREDIENT_QUANTITY.Where(b => b.PRODUCT_ID.Equals(id));
        }

        // PUT: api/DefaultIngredientQuantity/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDEFAULT_INGREDIENT_QUANTITY(int productId, int ingredientId, DEFAULT_INGREDIENT_QUANTITY dEFAULT_INGREDIENT_QUANTITY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if ((productId != dEFAULT_INGREDIENT_QUANTITY.PRODUCT_ID) && (ingredientId != dEFAULT_INGREDIENT_QUANTITY.INGREDIENT_ID))
            {
                return BadRequest();
            }

            db.Entry(dEFAULT_INGREDIENT_QUANTITY).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DEFAULT_INGREDIENT_QUANTITYExists(productId, ingredientId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DefaultIngredientQuantity
        [ResponseType(typeof(DEFAULT_INGREDIENT_QUANTITY))]
        public IHttpActionResult PostDEFAULT_INGREDIENT_QUANTITY(DEFAULT_INGREDIENT_QUANTITY dEFAULT_INGREDIENT_QUANTITY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DEFAULT_INGREDIENT_QUANTITY.Add(dEFAULT_INGREDIENT_QUANTITY);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DEFAULT_INGREDIENT_QUANTITYExists(dEFAULT_INGREDIENT_QUANTITY.PRODUCT_ID, dEFAULT_INGREDIENT_QUANTITY.INGREDIENT_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = dEFAULT_INGREDIENT_QUANTITY.PRODUCT_ID }, dEFAULT_INGREDIENT_QUANTITY);
        }

        // DELETE: api/DefaultIngredientQuantity/{productId}/{ingredientId}
        [Route("api/DefaultIngredientQuantity/{productId}/{ingredientId}")]
        [ResponseType(typeof(DEFAULT_INGREDIENT_QUANTITY))]
        public IHttpActionResult DeleteDEFAULT_INGREDIENT_QUANTITY(int productId, int ingredientId)
        {
            DEFAULT_INGREDIENT_QUANTITY dEFAULT_INGREDIENT_QUANTITY = db.DEFAULT_INGREDIENT_QUANTITY.Find(productId, ingredientId);
            if (dEFAULT_INGREDIENT_QUANTITY == null)
            {
                return NotFound();
            }

            db.DEFAULT_INGREDIENT_QUANTITY.Remove(dEFAULT_INGREDIENT_QUANTITY);
            db.SaveChanges();

            return Ok(dEFAULT_INGREDIENT_QUANTITY);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DEFAULT_INGREDIENT_QUANTITYExists(int productId, int ingredientId)
        {
            if ((db.DEFAULT_INGREDIENT_QUANTITY.Count(e => e.PRODUCT_ID == productId) > 0) && (db.DEFAULT_INGREDIENT_QUANTITY.Count(e => e.INGREDIENT_ID == ingredientId) > 0))
            {
                return true;
            }

            return false;
        }
    }
}