﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class ProductTypeController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/ProductType
        public IQueryable<PRODUCT_TYPE> GetPRODUCT_TYPE()
        {
            return db.PRODUCT_TYPE;
        }

        // GET: api/ProductType/5
        [ResponseType(typeof(PRODUCT_TYPE))]
        public IHttpActionResult GetPRODUCT_TYPE(string id)
        {
            PRODUCT_TYPE pRODUCT_TYPE = db.PRODUCT_TYPE.Find(id);
            if (pRODUCT_TYPE == null)
            {
                return NotFound();
            }

            return Ok(pRODUCT_TYPE);
        }

        // PUT: api/ProductType/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPRODUCT_TYPE(string id, PRODUCT_TYPE pRODUCT_TYPE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pRODUCT_TYPE.PRODUCT_TYPE_NAME)
            {
                return BadRequest();
            }

            db.Entry(pRODUCT_TYPE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PRODUCT_TYPEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProductType
        [ResponseType(typeof(PRODUCT_TYPE))]
        public IHttpActionResult PostPRODUCT_TYPE(PRODUCT_TYPE pRODUCT_TYPE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PRODUCT_TYPE.Add(pRODUCT_TYPE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PRODUCT_TYPEExists(pRODUCT_TYPE.PRODUCT_TYPE_NAME))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pRODUCT_TYPE.PRODUCT_TYPE_NAME }, pRODUCT_TYPE);
        }

        // DELETE: api/ProductType/5
        [ResponseType(typeof(PRODUCT_TYPE))]
        public IHttpActionResult DeletePRODUCT_TYPE(string id)
        {
            PRODUCT_TYPE pRODUCT_TYPE = db.PRODUCT_TYPE.Find(id);
            if (pRODUCT_TYPE == null)
            {
                return NotFound();
            }

            db.PRODUCT_TYPE.Remove(pRODUCT_TYPE);
            db.SaveChanges();

            return Ok(pRODUCT_TYPE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PRODUCT_TYPEExists(string id)
        {
            return db.PRODUCT_TYPE.Count(e => e.PRODUCT_TYPE_NAME == id) > 0;
        }
    }
}