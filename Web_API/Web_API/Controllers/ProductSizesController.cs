﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class ProductSizesController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/ProductSizes
        public IQueryable<PRODUCT_SIZES> GetPRODUCT_SIZES()
        {
            return db.PRODUCT_SIZES;
        }

        // GET: api/ProductSizes/5/5
        [Route("api/ProductSizes/{productId}/{sizeId}")]
        [ResponseType(typeof(PRODUCT_SIZES))]
        public IHttpActionResult GetPRODUCT_SIZES(int productId, int sizeId)
        {
            PRODUCT_SIZES pRODUCT_SIZES = db.PRODUCT_SIZES.Find(productId, sizeId);
            if (pRODUCT_SIZES == null)
            {
                return NotFound();
            }

            return Ok(pRODUCT_SIZES);
        }

        // PUT: api/ProductSizes/5
        [Route("api/ProductSizes/{productId}/{sizeId}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPRODUCT_SIZES(int productId, int sizeId, PRODUCT_SIZES pRODUCT_SIZES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if ((productId != pRODUCT_SIZES.PRODUCT_ID) && (sizeId != pRODUCT_SIZES.SIZE_ID))
            {
                return BadRequest();
            }

            db.Entry(pRODUCT_SIZES).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PRODUCT_SIZESExists(productId, sizeId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProductSizes
        [ResponseType(typeof(PRODUCT_SIZES))]
        public IHttpActionResult PostPRODUCT_SIZES(PRODUCT_SIZES pRODUCT_SIZES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PRODUCT_SIZES.Add(pRODUCT_SIZES);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PRODUCT_SIZESExists(pRODUCT_SIZES.PRODUCT_ID, pRODUCT_SIZES.SIZE_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pRODUCT_SIZES.PRODUCT_ID }, pRODUCT_SIZES);
        }

        // DELETE: api/ProductSizes/5/5
        [Route("api/ProductSizes/{productId}/{sizeId}")]
        [ResponseType(typeof(PRODUCT_SIZES))]
        public IHttpActionResult DeletePRODUCT_SIZES(int productId, int sizeId)
        {
            PRODUCT_SIZES pRODUCT_SIZES = db.PRODUCT_SIZES.Find(productId, sizeId);
            if (pRODUCT_SIZES == null)
            {
                return NotFound();
            }

            db.PRODUCT_SIZES.Remove(pRODUCT_SIZES);
            db.SaveChanges();

            return Ok(pRODUCT_SIZES);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PRODUCT_SIZESExists(int productId, int sizeId)
        {
            if ((db.PRODUCT_SIZES.Count(e => e.PRODUCT_ID == productId) > 0) && (db.PRODUCT_SIZES.Count(e => e.SIZE_ID == sizeId) > 0)) 
            {
                return true;
            }

            return false;
        }
    }
}