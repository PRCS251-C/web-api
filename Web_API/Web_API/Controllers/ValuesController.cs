﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Web_API.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            UTF8Encoding encoding = new UTF8Encoding();
            string startString = "Hello world!";
            byte[] encryptedArray = EncryptionHandler.getInstance().EncryptWithAES(startString);
            string encryptedString = encoding.GetString(encryptedArray);
            string decryptedString = EncryptionHandler.getInstance().DecryptWithAES(encryptedArray);
            return new string[] { startString, encryptedString, decryptedString };
        }

        [Route("api/Encrypt/Key")]
        public IEnumerable<string> getEncryptKey()
        {
            return EncryptionHandler.getInstance().getAESKey();
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
