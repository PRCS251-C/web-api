﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class CustomerController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/Customer
        public IQueryable<CUSTOMER> GetCUSTOMER()
        {
            return db.CUSTOMER;
        }

        [Route("api/Customer/Encrypted")]
        public string getEncryptedCustomer()
        {
            UTF8Encoding encoding = new UTF8Encoding();
            IQueryable<CUSTOMER> customerList = db.CUSTOMER;

            var json = JsonConvert.SerializeObject(customerList.ToArray());

            var encryptedJson = EncryptionHandler.getInstance().EncryptWithAES(json);
            string decryptedJson = EncryptionHandler.getInstance().DecryptWithAES(encryptedJson);

            JArray list = (JArray)JsonConvert.DeserializeObject(decryptedJson);

            return encoding.GetString(encryptedJson);
        }

        // GET: api/Customer/5
        [ResponseType(typeof(CUSTOMER))]
        public IHttpActionResult GetCUSTOMER(int id)
        {
            CUSTOMER cUSTOMER = db.CUSTOMER.Find(id);
            if (cUSTOMER == null)
            {
                return NotFound();
            }

            return Ok(cUSTOMER);
        }

        // GET: api/Customer/{username}
        [Route("api/Customer/Username/{username}")]
        [ResponseType(typeof(CUSTOMER))]
        public IQueryable<CUSTOMER> GetCustomerByUsername(string username)
        {
            return db.CUSTOMER.Where(b => b.CUSTOMER_USERNAME.Equals(username));
        }

        // GET: api/Customer/{username}/Salt
        [Route("api/Customer/{username}/Salt")]
        public IHttpActionResult getCustomerSalt(string username)
        {
            return Ok(db.CUSTOMER.Where(b => b.CUSTOMER_USERNAME.Equals(username)).Select(c => c.CUSTOMER_SEASONING));
        }

        [Route("api/Customer/Login")]
        public IHttpActionResult login(dynamic json)
        {
            JObject root = json;

            string username = (string)root["username"];
            string password = (string)root["password"];

            string storedPassword = db.CUSTOMER.Where(b => b.CUSTOMER_USERNAME.Equals(username)).Select(c => c.CUSTOMER_PASSWORD).First();

            if (password.Equals(storedPassword))
            {
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/Customer/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCUSTOMER(int id, CUSTOMER cUSTOMER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cUSTOMER.CUSTOMER_ID)
            {
                return BadRequest();
            }

            db.Entry(cUSTOMER).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CUSTOMERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Customer
        [ResponseType(typeof(CUSTOMER))]
        public IHttpActionResult PostCUSTOMER(CUSTOMER cUSTOMER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CUSTOMER.Add(cUSTOMER);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CUSTOMERExists(cUSTOMER.CUSTOMER_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(getLastId());
        }

        // DELETE: api/Customer/5
        [ResponseType(typeof(CUSTOMER))]
        public IHttpActionResult DeleteCUSTOMER(int id)
        {
            CUSTOMER cUSTOMER = db.CUSTOMER.Find(id);
            if (cUSTOMER == null)
            {
                return NotFound();
            }

            db.CUSTOMER.Remove(cUSTOMER);
            db.SaveChanges();

            return Ok(cUSTOMER);
        }

        [Route("api/Customer/LastId")]
        public int getLastId()
        {
            return db.CUSTOMER.Select(b => b.CUSTOMER_ID).Max();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CUSTOMERExists(int id)
        {
            return db.CUSTOMER.Count(e => e.CUSTOMER_ID == id) > 0;
        }
    }
}