﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class ProductController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/Product
        public IQueryable<PRODUCT> GetPRODUCT()
        {
            return db.PRODUCT;
        }

        // GET: api/Product/5
        [ResponseType(typeof(PRODUCT))]
        public IHttpActionResult GetPRODUCT(int id)
        {
            PRODUCT pRODUCT = db.PRODUCT.Find(id);
            if (pRODUCT == null)
            {
                return NotFound();
            }

            return Ok(pRODUCT);
        }

        // GET: api/Product/5/Ingredients
        [Route("api/Product/{id}/Ingredients")]
        [ResponseType(typeof(INGREDIENT))]
        public IQueryable<DEFAULT_INGREDIENT_QUANTITY> getIngredients(int id)
        {
            return db.DEFAULT_INGREDIENT_QUANTITY.Where(b => b.PRODUCT_ID.Equals(id));
        }

        [Route("api/Product/{id}/Sizes")]
        [ResponseType(typeof(PRODUCT_SIZES))]
        public IQueryable<PRODUCT_SIZES> getSizes(int id)
        {
            return db.PRODUCT_SIZES.Where(b => b.PRODUCT_ID.Equals(id));
        }

        // PUT: api/Product/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPRODUCT(int id, PRODUCT pRODUCT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pRODUCT.PRODUCT_ID)
            {
                return BadRequest();
            }

            db.Entry(pRODUCT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PRODUCTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("api/Product/PutAsWhole")]
        [HttpPut]
        public IHttpActionResult putProduct(dynamic json)
        {
            List<DEFAULT_INGREDIENT_QUANTITY> defaultIngredientList = new List<DEFAULT_INGREDIENT_QUANTITY>();
            List<PRODUCT_SIZES> productSizes = new List<PRODUCT_SIZES>();

            JObject root = json;

            JArray ingredientList = (JArray)root["INGREDIENT_LIST"];

            JArray productSizesList = (JArray)root["PRODUCT_SIZES"];

            PRODUCT product = root.ToObject<PRODUCT>();

            IEnumerable<DEFAULT_INGREDIENT_QUANTITY> currentIngredients = getIngredients(product.PRODUCT_ID);

            IEnumerable<PRODUCT_SIZES> currentSizes = getSizes(product.PRODUCT_ID);

            for (int i = 0; i < ingredientList.Count; i++)
            {
                DEFAULT_INGREDIENT_QUANTITY item = new DEFAULT_INGREDIENT_QUANTITY();

                item.INGREDIENT_ID = (int)ingredientList[i]["INGREDIENT_ID"];
                item.PRODUCT_ID = product.PRODUCT_ID;
                item.QUANTITY = (int)ingredientList[i]["quantity"];

                defaultIngredientList.Add(item);
            }

            for (int i = 0; i < productSizesList.Count; i++)
            {
                productSizes.Add(productSizesList[i].ToObject<PRODUCT_SIZES>());
            }

            IHttpActionResult productResult = PutPRODUCT(product.PRODUCT_ID, product);

            if (productResult.Equals(BadRequest()) || productResult.Equals(Conflict()))
            {
                return productResult;
            }

            IHttpActionResult result = Ok();

            foreach (DEFAULT_INGREDIENT_QUANTITY item in currentIngredients)
            {
                bool isContained = false;

                foreach (DEFAULT_INGREDIENT_QUANTITY newItem in defaultIngredientList)
                {
                    if (item.PRODUCT_ID.Equals(newItem.PRODUCT_ID) && item.INGREDIENT_ID.Equals(newItem.INGREDIENT_ID))
                    {
                        isContained = true;
                    }
                }

                if (!isContained)
                {
                    result = new DefaultIngredientQuantityController().DeleteDEFAULT_INGREDIENT_QUANTITY(item.PRODUCT_ID, item.INGREDIENT_ID);

                    if (result.Equals(NotFound()))
                    {
                        return result;
                    }
                }
            }

            foreach (DEFAULT_INGREDIENT_QUANTITY item in defaultIngredientList)
            {
                bool isContained = false;

                foreach (DEFAULT_INGREDIENT_QUANTITY newItem in currentIngredients)
                {
                    if (item.PRODUCT_ID.Equals(newItem.PRODUCT_ID) && item.INGREDIENT_ID.Equals(newItem.INGREDIENT_ID))
                    {
                        isContained = true;
                    }
                }

                if (isContained)
                {
                    result = new DefaultIngredientQuantityController().PutDEFAULT_INGREDIENT_QUANTITY(item.PRODUCT_ID, item.INGREDIENT_ID, item);
                } else
                {
                    result = new DefaultIngredientQuantityController().PostDEFAULT_INGREDIENT_QUANTITY(item);
                }

                if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                {
                    return result;
                }
            }

            foreach (PRODUCT_SIZES item in currentSizes)
            {
                bool isContained = false;

                foreach (PRODUCT_SIZES newItem in productSizes)
                {
                    if (item.PRODUCT_ID.Equals(newItem.PRODUCT_ID) && item.SIZE_ID.Equals(newItem.SIZE_ID))
                    {
                        isContained = true;
                    }
                }
                if (!isContained)
                {
                    result = new ProductSizesController().DeletePRODUCT_SIZES(item.PRODUCT_ID, item.SIZE_ID);
                }

                if (result.Equals(NotFound()))
                {
                    return result;
                }
            }

            foreach (PRODUCT_SIZES item in productSizes)
            {
                bool isContained = false;

                foreach (PRODUCT_SIZES newItem in currentSizes)
                {
                    if (item.PRODUCT_ID.Equals(newItem.PRODUCT_ID) && item.SIZE_ID.Equals(newItem.SIZE_ID))
                    {
                        isContained = true;
                    }
                }
                if (isContained)
                {
                    result = new ProductSizesController().PutPRODUCT_SIZES(item.PRODUCT_ID, item.SIZE_ID, item);
                } else
                {
                    result = new ProductSizesController().PostPRODUCT_SIZES(item);
                }

                if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                {
                    return result;
                }
            }

            return Ok();
        }

        // POST: api/Product
        [ResponseType(typeof(PRODUCT))]
        public IHttpActionResult PostPRODUCT(PRODUCT pRODUCT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PRODUCT.Add(pRODUCT);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PRODUCTExists(pRODUCT.PRODUCT_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        [Route("api/Product/PostAsWhole")]
        [HttpPost]
        public IHttpActionResult postProduct(dynamic json)
        {
            List<DEFAULT_INGREDIENT_QUANTITY> defaultIngredientList = new List<DEFAULT_INGREDIENT_QUANTITY>();
            List<PRODUCT_SIZES> productSizes = new List<PRODUCT_SIZES>();

            JObject root = json;

            JArray ingredientList = (JArray)root["INGREDIENT_LIST"];

            JArray productSizesList = (JArray)root["PRODUCT_SIZES"];

            PRODUCT product = root.ToObject<PRODUCT>();

            IHttpActionResult productResult = PostPRODUCT(product);

            if (productResult.Equals(BadRequest()) || productResult.Equals(Conflict()))
            {
                return productResult;
            }

            product.PRODUCT_ID = getLastId();

            for (int i = 0; i < ingredientList.Count; i++)
            {
                DEFAULT_INGREDIENT_QUANTITY item = new DEFAULT_INGREDIENT_QUANTITY();

                item.INGREDIENT_ID = (int)ingredientList[i]["INGREDIENT_ID"];
                item.PRODUCT_ID = product.PRODUCT_ID;
                item.QUANTITY = (int)ingredientList[i]["quantity"];

                defaultIngredientList.Add(item);
            }

            for (int i = 0; i < productSizesList.Count; i++)
            {
                productSizesList[i]["PRODUCT_ID"] = product.PRODUCT_ID;
                productSizes.Add(productSizesList[i].ToObject<PRODUCT_SIZES>());
            }

            foreach (DEFAULT_INGREDIENT_QUANTITY item in defaultIngredientList)
            {
                IHttpActionResult result = new DefaultIngredientQuantityController().PostDEFAULT_INGREDIENT_QUANTITY(item);

                if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                {
                    return result;
                }
            }

            foreach (PRODUCT_SIZES item in productSizes)
            {
                item.PRODUCT_ID = product.PRODUCT_ID; //Product ID is generated on the database, need to retrieve and update here

                IHttpActionResult result = new ProductSizesController().PostPRODUCT_SIZES(item);

                if (result.Equals(BadRequest()) || result.Equals(Conflict()))
                {
                    return result;
                }
            }

            return Ok(product.PRODUCT_ID);
        }

        // DELETE: api/Product/5
        [ResponseType(typeof(PRODUCT))]
        public IHttpActionResult DeletePRODUCT(int id)
        {
            PRODUCT pRODUCT = db.PRODUCT.Find(id);
            if (pRODUCT == null)
            {
                return NotFound();
            }

            db.PRODUCT.Remove(pRODUCT);
            db.SaveChanges();

            return Ok(pRODUCT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Route("api/Product/LastId")]
        public int getLastId()
        {
            return db.PRODUCT.Select(b => b.PRODUCT_ID).Max();
        }

        private bool PRODUCTExists(int id)
        {
            return db.PRODUCT.Count(e => e.PRODUCT_ID == id) > 0;
        }
    }
}