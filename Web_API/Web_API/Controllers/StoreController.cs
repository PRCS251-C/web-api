﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class StoreController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/Store
        public IQueryable<STORES> GetSTORES()
        {
            return db.STORES;
        }

        // GET: api/Store/5
        [ResponseType(typeof(STORES))]
        public IHttpActionResult GetSTORES(int id)
        {
            STORES sTORES = db.STORES.Find(id);
            if (sTORES == null)
            {
                return NotFound();
            }

            return Ok(sTORES);
        }

        // PUT: api/Store/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSTORES(int id, STORES sTORES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sTORES.STORE_ID)
            {
                return BadRequest();
            }

            db.Entry(sTORES).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!STORESExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Store
        [ResponseType(typeof(STORES))]
        public IHttpActionResult PostSTORES(STORES sTORES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.STORES.Add(sTORES);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (STORESExists(sTORES.STORE_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(getLastId());
        }

        // DELETE: api/Store/5
        [ResponseType(typeof(STORES))]
        public IHttpActionResult DeleteSTORES(int id)
        {
            STORES sTORES = db.STORES.Find(id);
            if (sTORES == null)
            {
                return NotFound();
            }

            db.STORES.Remove(sTORES);
            db.SaveChanges();

            return Ok(sTORES);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Route("api/Store/LastId")]
        public int getLastId()
        {
            return db.STORES.Select(b => b.STORE_ID).Max();
        }

        private bool STORESExists(int id)
        {
            return db.STORES.Count(e => e.STORE_ID == id) > 0;
        }
    }
}