﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API.Models;

namespace Web_API.Controllers
{
    public class IngredientController : ApiController
    {
        private PRCS251C_Model db = new PRCS251C_Model();

        // GET: api/Ingredient
        public IQueryable<INGREDIENT> GetINGREDIENT()
        {
            return db.INGREDIENT;
        }

        // GET: api/Ingredient/5
        [ResponseType(typeof(INGREDIENT))]
        public IHttpActionResult GetINGREDIENT(int id)
        {
            INGREDIENT iNGREDIENT = db.INGREDIENT.Find(id);
            if (iNGREDIENT == null)
            {
                return NotFound();
            }

            return Ok(iNGREDIENT);
        }

        // PUT: api/Ingredient/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutINGREDIENT(int id, INGREDIENT iNGREDIENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != iNGREDIENT.INGREDIENT_ID)
            {
                return BadRequest();
            }

            db.Entry(iNGREDIENT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!INGREDIENTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Ingredient
        [ResponseType(typeof(INGREDIENT))]
        public IHttpActionResult PostINGREDIENT(INGREDIENT iNGREDIENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.INGREDIENT.Add(iNGREDIENT);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (INGREDIENTExists(iNGREDIENT.INGREDIENT_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(getLastId());
        }

        // DELETE: api/Ingredient/5
        [ResponseType(typeof(INGREDIENT))]
        public IHttpActionResult DeleteINGREDIENT(int id)
        {
            INGREDIENT iNGREDIENT = db.INGREDIENT.Find(id);
            if (iNGREDIENT == null)
            {
                return NotFound();
            }

            db.INGREDIENT.Remove(iNGREDIENT);
            db.SaveChanges();

            return Ok(iNGREDIENT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Route("api/Ingredient/LastId")]
        public int getLastId()
        {
            return db.INGREDIENT.Select(b => b.INGREDIENT_ID).Max();
        }

        private bool INGREDIENTExists(int id)
        {
            return db.INGREDIENT.Count(e => e.INGREDIENT_ID == id) > 0;
        }
    }
}