﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace Web_API
{
    public class BlockingQueue
    {
        private static BlockingQueue instance;

        public bool isOrderPostBlocked { get; set; }
        public bool isOrderPutBlocked { get; set; }

        protected BlockingQueue()
        {
        }

        public static BlockingQueue getInstance()
        {
            if (instance == null)
            {
                instance = new BlockingQueue();
            }

            return instance;
        }
    }
}